﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JsonConfig.Models;

namespace JsonConfig
{
    public sealed class ConfigurationManager : ConfigurationModel
    {
        #region cTor
        private ConfigurationManager() : base()
        {
            base.OpenConfigurationFile();
        }

        private ConfigurationManager(IConfigEncryptionMethod encryptionMethod) : base(encryptionMethod)
        {
            base.OpenConfigurationFile();
        }
        #endregion

        #region Methods
        public override ConfigurationModel LoadConfiguration()
        {
            if (base.SingletonInstance != null) return base.SingletonInstance;
            return SingletonInstance = new ConfigurationManager();
        }
        public override ConfigurationModel LoadConfiguration(IConfigEncryptionMethod encryptionMethod)
        {
            if (base.SingletonInstance != null)
            {
                base.Encryption = encryptionMethod;
                return base.SingletonInstance;
            }
            return SingletonInstance = new ConfigurationManager(encryptionMethod);
        }

        
        #endregion
    }
}
