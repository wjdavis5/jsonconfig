﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
namespace JsonConfig.Models
{
     public abstract partial class ConfigurationModel :  IBaseObject
    {
        #region Properties
        public IConfigEncryptionMethod Encryption { get; set; }
        public Dictionary<string,IConfigElement> AppSettings { get; private set; }
        public static readonly string CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;
        public const string DefaultConfigFileName = "config.json";
        public string ConfigFilePath { get; private set; }
        public string RawJson { get; private set; }
        protected ConfigurationModel SingletonInstance { get; set; }
        #endregion

        #region cTor

        protected ConfigurationModel() : base()
        {
            AppSettings = new Dictionary<string, IConfigElement>();
            OpenConfigurationFile();
        }
        protected ConfigurationModel(IConfigEncryptionMethod encryptionMethod) : base()
        {
            AppSettings = new Dictionary<string, IConfigElement>();
            Encryption = encryptionMethod;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Looks in the current directory for the applications config file
        /// </summary>
        /// <returns></returns>
        public abstract ConfigurationModel LoadConfiguration();
        public abstract ConfigurationModel LoadConfiguration(IConfigEncryptionMethod encryptionMethod);

        public virtual string OpenConfigurationFile()
        {
            ConfigFilePath = Directory.GetFiles(CurrentDirectory, DefaultConfigFileName).FirstOrDefault();
            if (ConfigFilePath != null) return RawJson = File.ReadAllText(ConfigFilePath);
            throw new ConfigFileNotFound(string.Format("Cannot locate {} in {}",DefaultConfigFileName,CurrentDirectory));
        }


        #region OverRides
        public override bool Equals(object obj)
        {
            return (string) obj == RawJson;
        }
        public override int GetHashCode()
        {
            return RawJson.GetHashCode();
        }
        public override string ToString()
        {
            return RawJson;
        }
        #endregion

        #endregion
    }
}


