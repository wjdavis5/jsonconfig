﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonConfig.Models
{
     public abstract partial class ConfigurationModel :  IBaseObject
     {
         public class ConfigFileNotFound : Exception
         {             
             public ConfigFileNotFound() : base(){}
             public ConfigFileNotFound(string message):base(message){}
             public ConfigFileNotFound(string message,Exception inner):base(message,inner){}
         }

     }
}
