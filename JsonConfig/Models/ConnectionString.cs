﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonConfig.Models
{
    class ConnectionString : IConfigElement
    {
        public bool IsEncrypted { get; set; }
        public string Value { get; set; }
    }
}
