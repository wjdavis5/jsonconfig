﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonConfig.Models
{
    public abstract class IBaseObject
    {
        public DateTime CreatedDate { get; private set; }
        public DateTime ModifiedDate { get; private set; }

        protected IBaseObject()
        {
            CreatedDate = DateTime.Now;
        }
        public void Update()
        {
            ModifiedDate = DateTime.Now;
        }



    }
}
