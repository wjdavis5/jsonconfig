﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonConfig.Models
{
    public interface IConfigEncryptionMethod
    {
        string Encrypt(string value);
        string Decrypt(string value);
    }
}
